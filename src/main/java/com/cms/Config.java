package com.cms;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Config implements Serializable {

	private static final long serialVersionUID = 1012463140749529805L;

	/** 缓存名称 */
	public static final String CACHE_NAME = "config";

	/** 网站名称 */
	private String siteName;
	
	/** 网站网址 */
	private String siteUrl;
	
	/** logo */
	private String logo;
	
	/** 标题 */
	private String title;
	
	/** 关键词 */
	private String keywords;
	
	/** 描述 */
	private String description;
	
	/** 主题 */
	private String theme;
	
	private String searchWord;
	
	private String imgUrl;
	
	private String oss_endpoint;
	private String oss_accessId;
	private String oss_accessKey;
	private String oss_bucket;
	
	private String sms_signName;
	private String sms_accessKey;
	private String sms_accessSecret;
	
	private String wuliu_code;
	
	
	private String weixin_wechatAppId;
	private String weixin_wechatSecret;
	private String weixin_xcxAppId;
	private String weixin_xcxSecret;
	private String weixin_openAppId;
	private String weixin_mchId;
	private String weixin_mchAppId;
	private String weixin_key;
	
	private String alipay_appId;
	private String alipay_appPrivateKey;
	private String alipay_publicKey;
	
	
	
    /**
	 * 获取网站名称
	 * 
	 * @return 网站名称
	 */
	public String getSiteName() {
		return siteName;
	}

	/**
	 * 设置网站名称
	 * 
	 * @param siteName
	 *            网站名称
	 */
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	/**
	 * 获取网站网址
	 * 
	 * @return 网站网址
	 */
	public String getSiteUrl() {
		return siteUrl;
	}

	/**
	 * 设置网站网址
	 * 
	 * @param siteUrl
	 *            网站网址
	 */
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	/**
	 * 获取logo
	 * 
	 * @return logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * 设置logo
	 * 
	 * @param logo
	 *            logo
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}
	
	/**
	 * 获取标题
	 * 
	 * @return 标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置标题
	 * 
	 * @param title
	 *            标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取关键词
	 * 
	 * @return 关键词
	 */
	public String getKeywords() {
		return keywords;
	}

	/**
	 * 设置关键词
	 * 
	 * @param keywords
	 *            关键词
	 */
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * 获取描述
	 * 
	 * @return 描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置描述
	 * 
	 * @param description
	 *            描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取主题
	 * 
	 * @return 主题
	 */
	public String getTheme() {
		return theme;
	}

	/**
	 * 设置主题
	 * 
	 * @param theme
	 *            主题
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getOss_endpoint() {
		return oss_endpoint;
	}

	public void setOss_endpoint(String oss_endpoint) {
		this.oss_endpoint = oss_endpoint;
	}

	public String getOss_accessId() {
		return oss_accessId;
	}

	public void setOss_accessId(String oss_accessId) {
		this.oss_accessId = oss_accessId;
	}

	public String getOss_accessKey() {
		return oss_accessKey;
	}

	public void setOss_accessKey(String oss_accessKey) {
		this.oss_accessKey = oss_accessKey;
	}

	public String getOss_bucket() {
		return oss_bucket;
	}

	public void setOss_bucket(String oss_bucket) {
		this.oss_bucket = oss_bucket;
	}

	public String getSms_signName() {
		return sms_signName;
	}

	public void setSms_signName(String sms_signName) {
		this.sms_signName = sms_signName;
	}

	public String getSms_accessKey() {
		return sms_accessKey;
	}

	public void setSms_accessKey(String sms_accessKey) {
		this.sms_accessKey = sms_accessKey;
	}

	public String getSms_accessSecret() {
		return sms_accessSecret;
	}

	public void setSms_accessSecret(String sms_accessSecret) {
		this.sms_accessSecret = sms_accessSecret;
	}

	public String getWuliu_code() {
		return wuliu_code;
	}

	public void setWuliu_code(String wuliu_code) {
		this.wuliu_code = wuliu_code;
	}

	public String getWeixin_wechatAppId() {
		return weixin_wechatAppId;
	}

	public void setWeixin_wechatAppId(String weixin_wechatAppId) {
		this.weixin_wechatAppId = weixin_wechatAppId;
	}

	public String getWeixin_wechatSecret() {
		return weixin_wechatSecret;
	}

	public void setWeixin_wechatSecret(String weixin_wechatSecret) {
		this.weixin_wechatSecret = weixin_wechatSecret;
	}

	public String getWeixin_xcxAppId() {
		return weixin_xcxAppId;
	}

	public void setWeixin_xcxAppId(String weixin_xcxAppId) {
		this.weixin_xcxAppId = weixin_xcxAppId;
	}

	public String getWeixin_xcxSecret() {
		return weixin_xcxSecret;
	}

	public void setWeixin_xcxSecret(String weixin_xcxSecret) {
		this.weixin_xcxSecret = weixin_xcxSecret;
	}

	public String getWeixin_openAppId() {
		return weixin_openAppId;
	}

	public void setWeixin_openAppId(String weixin_openAppId) {
		this.weixin_openAppId = weixin_openAppId;
	}

	public String getWeixin_mchId() {
		return weixin_mchId;
	}

	public void setWeixin_mchId(String weixin_mchId) {
		this.weixin_mchId = weixin_mchId;
	}

	public String getWeixin_mchAppId() {
		return weixin_mchAppId;
	}

	public void setWeixin_mchAppId(String weixin_mchAppId) {
		this.weixin_mchAppId = weixin_mchAppId;
	}

	public String getWeixin_key() {
		return weixin_key;
	}

	public void setWeixin_key(String weixin_key) {
		this.weixin_key = weixin_key;
	}

	public String getAlipay_appId() {
		return alipay_appId;
	}

	public void setAlipay_appId(String alipay_appId) {
		this.alipay_appId = alipay_appId;
	}

	public String getAlipay_appPrivateKey() {
		return alipay_appPrivateKey;
	}

	public void setAlipay_appPrivateKey(String alipay_appPrivateKey) {
		this.alipay_appPrivateKey = alipay_appPrivateKey;
	}

	public String getAlipay_publicKey() {
		return alipay_publicKey;
	}

	public void setAlipay_publicKey(String alipay_publicKey) {
		this.alipay_publicKey = alipay_publicKey;
	}
}
