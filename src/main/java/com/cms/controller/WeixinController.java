package com.cms.controller;

import java.util.Map;

import com.cms.routes.RouteMapping;
import com.cms.util.WeixinUtils;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;

@RouteMapping(url = "/weixin")
public class WeixinController extends Controller{

    public void index(){
        String echostr = getPara("echostr");
        String timestamp = getPara("timestamp");
        String signature = getPara("signature");
        //第一次验证域名
//        if(StringUtils.isNotBlank(echostr)){
//            renderJson(echostr);
//            return;
//        }
        System.out.println("==进入微信==");
        String data = HttpKit.readData(getRequest());
        System.out.println("==data=="+data);
        try {
            Map<String, String> map = WeixinUtils.xmlToMap(data);
            System.out.println("map=="+map);
            String event = map.get("Event");
            //发送方帐号（一个OpenID）
            String fromUserName = map.get("FromUserName");
            //开发者微信号
            String toUserName = map.get("ToUserName");
            String eventKey = map.get("EventKey");
            switch (event.toLowerCase()) {
                case "click":
                    System.out.println("click:"+eventKey);
                    break;
                case "subscribe":
                	System.out.println("subscribe:"+eventKey);
                    break;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            renderJson("success");
        }
    }
    
    /**
     * 菜单
     */
    public void menu(){
        String json="";
        json+="{";
        json+="\"button\":[";
        json+="{";
        json+="\"name\":\"分销\",";
        json+="\"sub_button\":[";
        json+="{";    
        json+="\"type\":\"click\",";
        json+="\"name\":\"我的海报\",";
        json+="\"key\":\"poster\"";
        json+="},";
        json+="{";    
        json+="\"type\":\"view\",";
        json+="\"name\":\"我的收益\",";
        json+="\"url\":\"http://shop.jrecms.com/fx/weixin/login\"";
        json+="}";
        json+="]";
        json+="}";
        json+="]";
        json+="}";
        System.out.println(json);
        WeixinUtils.menuCreate(json);
    }
}
