/*
 * 
 * 
 * 
 */
package com.cms.controller.admin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.cms.CommonAttribute;
import com.jfinal.kit.PathKit;
import org.apache.commons.io.FilenameUtils;

import com.cms.Config;
import com.cms.routes.RouteMapping;
import com.cms.util.OSSUtils;
import com.cms.util.SystemUtils;
import com.jfinal.upload.UploadFile;

/**
 * Controller - 文件
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/file")

public class FileController extends BaseController {

	/**
	 * 上传
	 */
	public void upload() {
		UploadFile uploadFile = getFile();
		Map<String, Object> data = new HashMap<String, Object>();
		if (uploadFile == null || uploadFile.getFile().length()==0) {
			data.put("message", "操作错误");
			data.put("state", "ERROR");
			renderJson(data);
			return;
		}
		String newFileName = UUID.randomUUID().toString()+"."+FilenameUtils.getExtension(uploadFile.getOriginalFileName());
		//本地存储
		uploadFile.getFile().renameTo(new File(PathKit.getWebRootPath()+"/"+CommonAttribute.UPLOAD_PATH+"/"+newFileName));
		//OSS
		//OSSUtils.upload(newFileName,uploadFile.getFile(), uploadFile.getContentType());
		data.put("message", "成功");
		data.put("state", "SUCCESS");
		data.put("url", SystemUtils.getConfig().getImgUrl()+newFileName);
		data.put("name",newFileName);
		//删除文件
		uploadFile.getFile().delete();
		renderJson(data);
	}
}