package com.cms.controller.admin;

import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;

import com.cms.Feedback;
import com.cms.entity.MemberRank;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/admin/member_rank")
public class MemberRankController extends BaseController {

	/**
	 * 添加
	 */
	public void add() {
		render(getView("member_rank/add"));
	}

	/**
	 * 保存
	 */
	public void save() {
		MemberRank memberRank = getModel(MemberRank.class,"",true); 
		memberRank.setCreateDate(new Date());
		memberRank.setModifyDate(new Date());
		memberRank.save();
		redirect(getListQuery("/admin/member_rank/list"));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Long id = getParaToLong("id");
		setAttr("memberRank", new MemberRank().dao().findById(id));
		render(getView("member_rank/edit"));
	}

	/**
	 * 更新
	 */
	public void update() {
		MemberRank memberRank = getModel(MemberRank.class,"",true); 
		memberRank.setModifyDate(new Date());
		memberRank.update();
		redirect(getListQuery("/admin/member_rank/list"));
	}
	
	/**
	 * 列表
	 */
	public void list() {
	    String name = getPara("name");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new MemberRank().dao().findPage(name,pageNumber,PAGE_SIZE));
		setAttr("name", name);
		render(getView("member_rank/list"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Long ids[] = getParaValuesToLong("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Long id:ids){
				new MemberRank().dao().deleteById(id);
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}
}
