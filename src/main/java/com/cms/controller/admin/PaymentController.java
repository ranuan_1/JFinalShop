package com.cms.controller.admin;

import com.cms.entity.Order;
import com.cms.entity.Payment;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/admin/payment")
public class PaymentController extends BaseController {
	
	public void list() {
	    String nickname = getPara("nickname");
	    String mobile = getPara("mobile");
		Payment.Type type = getParaToEnum(Payment.Type.class,"type");
		Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("page", new Payment().dao().findPage(nickname,mobile,type,pageNumber,PAGE_SIZE));
		setAttr("nickname", nickname);
		setAttr("mobile", mobile);
		if(type!=null){
			setAttr("type", type.name());
		}
		render(getView("payment/list"));
	}

}
