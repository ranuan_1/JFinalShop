package com.cms.controller.admin;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import com.cms.Feedback;
import com.cms.entity.Member;
import com.cms.entity.Order;
import com.cms.entity.Payment;
import com.cms.entity.Withdraw;
import com.cms.routes.RouteMapping;

/**
 * Controller - 提现
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/withdraw")
public class WithdrawController extends BaseController {

    /**
     * 完成
     */
    public void updateStatus(){
        Long withdrawId = getParaToLong("withdrawId");
        Withdraw.Status status = getParaToEnum(Withdraw.Status.class,"status");
        Withdraw withdraw = new Withdraw().dao().findById(withdrawId);
        withdraw.setStatus(status.ordinal());
        withdraw.update();
        if(Withdraw.Status.REJECTED.ordinal()==status.ordinal()){
        	Payment payment = new Payment();
    		payment.setType(Payment.Type.REFUND.ordinal());
    		payment.setCreateDate(new Date());
    		payment.setModifyDate(new Date());
    		payment.setMemberId(withdraw.getMemberId());
    		payment.setOrderId(null);
    		payment.setAmount(withdraw.getAmount());
    		payment.setInout("+");
    		payment.save();
    		
    		Member member = new Member().dao().findById(withdraw.getMemberId());
    		BigDecimal newAmount = member.getAmount().add(withdraw.getAmount());
    		member.setAmount(newAmount);
    		member.update();
        }
        renderJson(Feedback.success(new HashMap<>()));
    }
    
    /**
     * 列表
     */
    public void list(){
    	Withdraw.Status status = getParaToEnum(Withdraw.Status.class,"status");
        Integer pageNumber = getParaToInt("pageNumber");
        if(pageNumber==null){
            pageNumber = 1;
        }
        setAttr("page", new Withdraw().dao().findPage(status,pageNumber,PAGE_SIZE));
        setAttr("status", status);
        render(getView("withdraw/list"));
    }
}
