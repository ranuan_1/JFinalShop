package com.cms.controller.commmon;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayConstants;
import com.alipay.api.internal.util.AlipaySignature;
import com.cms.Config;
import com.cms.entity.Order;
import com.cms.entity.Payment;
import com.cms.routes.RouteMapping;
import com.cms.util.DistributeUtils;
import com.cms.util.SystemUtils;
import com.cms.util.WeixinUtils;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import net.sf.ehcache.search.expression.Or;

@RouteMapping(url = "/common/pay_notify")
public class PayNotifyController extends Controller{
	
	
    /**
     * 支付宝异步
     */
    public void alipayNotify(){
        //获取支付宝POST过来反馈信息
    	Config config = SystemUtils.getConfig();
        Map<String,String> params = new HashMap<String,String>();
        Map requestParams = getRequest().getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i]
                            : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }
        System.out.println("==alipayNotify==="+params);
        //切记alipaypublickey是支付宝的公钥，请去open.alipay.com对应应用下查看。
        //boolean AlipaySignature.rsaCheckV1(Map<String, String> params, String publicKey, String charset, String sign_type)
        try {
            boolean flag = AlipaySignature.rsaCheckV1(params, config.getAlipay_publicKey(), AlipayConstants.CHARSET_UTF8,AlipayConstants.SIGN_TYPE_RSA2);
            if(flag){
                //插入流水
                String trade_status = params.get("trade_status");
                String out_trade_no = params.get("out_trade_no");
                BigDecimal total_amount = new BigDecimal(params.get("total_amount"));
                String passback_params = params.get("passback_params");
                String attach = "";
                try {
                	attach = URLDecoder.decode(passback_params,"UTF-8");
                } catch (UnsupportedEncodingException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if("TRADE_SUCCESS".equals(trade_status) || "TRADE_FINISHED".equals(trade_status)){
                    new Order().paymentComplete(out_trade_no);
                    //返回
                    renderJson("success");
                }
            }
        } catch (AlipayApiException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
   /**
     * 微信支付异步
     */
    public void weixinpayNotify(){
    	Config config = SystemUtils.getConfig();
        String data = HttpKit.readData(getRequest());
        Map<String,String> weixinResult = new HashMap<String,String>();
        try {
            System.out.println("==weixinpayNotify=data="+data);
            Map<String,String> result = WeixinUtils.xmlToMap(data);
            System.out.println("==weixinpayNotify=result="+result);
            //判断验签
            if(WeixinUtils.isSignatureValid(data, config.getWeixin_key())){
                String out_trade_no = result.get("out_trade_no");
                String result_code = result.get("result_code");
                String attach = result.get("attach");
                if("SUCCESS".equals(result_code)){
                    new Order().paymentComplete(out_trade_no);
                    //返回
                    weixinResult.put("return_code", "SUCCESS");
                    weixinResult.put("return_msg", "OK");
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        try {
            renderText(WeixinUtils.mapToXml(weixinResult));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    

}
