package com.cms.controller.front.member;

import java.util.HashMap;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Member;
import com.cms.entity.MessageConfig;
import com.cms.entity.SafeKey;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/account")
public class AccountController extends BaseController{
	
	public void index(){
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_account.html");
	}
	
	public void save(){
		Member member = new Member().dao().findById(getCurrentMember().getId());
		Member.AccountType accountType = getParaToEnum(Member.AccountType.class,"accountType");
		String accountName = getPara("accountName");
		String bankName = getPara("bankName");
		String account = getPara("account");
		String code = getPara("code");
		SafeKey safeKey = new SafeKey().dao().findByMobile(member.getMobile(), MessageConfig.Type.SET_ACCOUNT.ordinal());
		if(!(safeKey.getValue().equals(code))){
			renderJson(Feedback.error("短信验证码错误"));
			return;
		}
		member.setAccountName(accountName);
		member.setBankName(bankName);
		member.setAccountType(accountType.ordinal());
		member.setAccount(account);
		member.update();
		renderJson(Feedback.success(new HashMap<>()));
	}

}
