package com.cms.controller.front.member;

import com.cms.controller.front.BaseController;
import com.cms.routes.RouteMapping;

@RouteMapping(url = "/member/recharge")
public class RechargeController extends BaseController{

	public void index(){
		render("/templates/"+getTheme()+"/"+getDevice()+"/member_recharge.html");
	}
	
}
