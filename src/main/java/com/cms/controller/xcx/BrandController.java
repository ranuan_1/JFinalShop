package com.cms.controller.xcx;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cms.Feedback;
import com.cms.controller.front.BaseController;
import com.cms.entity.Brand;
import com.cms.routes.RouteMapping;

/**
 * @description : 
 * @author : heyewei
 * @create : 2020年9月22日
 **/
@RouteMapping(url = "/xcx/brand")
public class BrandController extends BaseController{

	
	public void index(){
		List<Brand> brands = new Brand().dao().find("select * from cms_brand ");
		Map<String,Object> map = new HashMap<>();
		map.put("brands", brands);
		renderJson(Feedback.success(map));
	}
	
	public void detail(){
		Long id = getParaToLong("id");
		Brand brand = new Brand().dao().findById(id);
		Map<String,Object> map = new HashMap<>();
		map.put("brand", brand);
		map.put("products", brand.getProducts());
		renderJson(Feedback.success(map));
	}
}
