package com.cms.job;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import com.cms.entity.Commission;
import com.cms.entity.Order;

public class CommissionCompletedJob implements Runnable{

    @Override
    public void run() {
        // TODO Auto-generated method stub
        Date beginDate = new Date();
        Date endDate = DateUtils.addDays(beginDate, 7);
        List<Order> orders = new Order().dao().findList(Order.Status.COMPLETED.ordinal(),beginDate,endDate);
        for(Order order : orders){
        	List<Commission> commissions = new Commission().dao().findByOrderId(order.getId());
            for(Commission commission : commissions){
                commission.setModifyDate(new Date());
                commission.setStatus(1);
                commission.update();
            }
        }
    }

}
