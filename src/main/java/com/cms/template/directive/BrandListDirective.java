package com.cms.template.directive;

import java.util.List;

import com.cms.TemplateVariable;
import com.cms.entity.Brand;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 模板指令 - 品牌列表
 * 
 * 
 * 
 */
@TemplateVariable(name="brand_list")
public class BrandListDirective extends BaseDirective {

	/** 变量名称 */
	private static final String VARIABLE_NAME = "brands";

    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Integer start = getStart(scope);
        Integer count = getCount(scope);
        String orderBy = getOrderBy(scope);
        List<Brand> brands = new Brand().dao().findList(orderBy,start,count);
        scope.setLocal(VARIABLE_NAME,brands);
        stat.exec(env, scope, writer);
    }

    public boolean hasEnd() {
        return true;
    }
}