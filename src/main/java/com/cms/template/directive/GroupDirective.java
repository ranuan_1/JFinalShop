package com.cms.template.directive;

import com.cms.TemplateVariable;
import com.cms.entity.Group;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;

/**
 * 
 * 
 * 
 */
@TemplateVariable(name="group")
public class GroupDirective extends BaseDirective {

	/** "ID"参数名称 */
	private static final String ID_PARAMETER_NAME = "id";
	
	/** 变量名称 */
	private static final String VARIABLE_NAME = "group";
	
    @Override
    public void exec(Env env, Scope scope, Writer writer) {
        // TODO Auto-generated method stub
        scope = new Scope(scope);
        Long id = getParameter(ID_PARAMETER_NAME, Long.class, scope);
        Group group = new Group().dao().findById(id);
        scope.setLocal(VARIABLE_NAME,group);
        stat.exec(env, scope, writer);
    }
    
    public boolean hasEnd() {
        return true;
    }
}