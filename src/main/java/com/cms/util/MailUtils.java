package com.cms.util;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dm.model.v20151123.SingleSendMailRequest;
import com.aliyuncs.dm.model.v20151123.SingleSendMailResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.cms.Config;
import com.cms.entity.MessageConfig;
import com.jfinal.template.Engine;
/**
 * Utils - 邮件
 * 
 * 
 * 
 */
public class MailUtils {
    
      /** 邮件accessKey */
      private static String accessKey = "";
    
      /** 邮件accessSecret */
      private static String accessSecret = "";
      
      /** 邮件标签 */
      private static String tagName = "";
      
      /** 邮件发信地址 */
      private static String accountName = "";
      
      /** 邮件发信人昵称 */
      private static String fromAlias = "";

	/**
	 * 添加邮件发送任务
	 * 
	 * @param toMails
	 *            收件人邮箱
	 * @param subject
	 *            主题
	 * @param content
	 *            内容
	 */
	private static void addSendTask(final String[] toMails, final String subject, final String content) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				send(toMails, subject, content);
			}
		});
		thread.start();
	}

	/**
	 * 发送邮件
	 * 
	 * @param toMails
	 *            收件人邮箱
	 * @param subject
	 *            主题
	 * @param content
	 *            内容
	 */
	private static void send(String[] toMails, String subject, String content) {
		try {
			IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKey, accessSecret);
	        IAcsClient client 	= new DefaultAcsClient(profile);
	        SingleSendMailRequest request = new SingleSendMailRequest();
            request.setAccountName(accountName);
             request.setFromAlias(fromAlias);
            request.setAddressType(1);
            request.setTagName(tagName);
            request.setReplyToAddress(true);
            request.setToAddress(StringUtils.join(toMails, ","));
            request.setSubject(subject);
            request.setHtmlBody(content);
            SingleSendMailResponse httpResponse = client.getAcsResponse(request);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}

	/**
	 * 发送邮件
	 * 
	 * @param toMails
	 *            收件人邮箱
	 * @param subject
	 *            主题
	 * @param templatePath
	 *            模板路径
	 * @param params
	 *            数据
	 * @param async
	 *            是否异步
	 */
	public static void send(String[] toMails, String subject, String templateContent, Map<String, Object> params, boolean async) {
		try{
			String content = Engine.use().getTemplateByString(templateContent).renderToString(params);
			send( toMails, subject, content, async);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * 发送邮件
	 * 
	 * @param toMails
	 *            收件人邮箱
	 * @param subject
	 *            主题
	 * @param content
	 *            内容
	 * @param async
	 *            是否异步
	 */
	public static void send(String[] toMails, String subject, String content, boolean async) {
		if (async) {
			addSendTask(toMails, subject, content);
		} else {
			send(toMails, subject, content);
		}
	}


	/**
	 * 发送邮件(异步)
	 * 
	 * @param toMail
	 *            收件人邮箱
	 * @param subject
	 *            主题
	 * @param content
	 *            内容
	 */
	public static void send(String toMail, String subject, String content) {
		send(new String[] { toMail }, subject, content, true);
	}
	
	/**
	 * 发送邮件(异步)
	 * 
	 * @param toMail
	 *            收件人邮箱
	 * @param subject
	 *            主题
	 * @param templateContent
	 *            模板内容
	 * @param model
	 *            数据
	 */
	public static void send(String toMail, String subject, String templateContent, Map<String, Object> params) {
		send(new String[] { toMail }, subject, templateContent, params, true);
	}

	/**
	 * 发送密码找回邮件
	 * 
	 * @param toMail
	 *            收件人邮箱
	 * @param username
	 *            用户名
	 * @param safeKeyValue
	 *            安全密匙值
	 * @param safeKeyExpire
	 *            安全密匙到期时间     
	 */
	public static void sendForgetPasswordMail(String code,String email) {
		MessageConfig messageConfig = new MessageConfig().dao().findByType(MessageConfig.Type.FORGET_PASSWORD.ordinal());
		if (messageConfig == null) {
			return;
		}
		Config config = SystemUtils.getConfig();
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("code", code);
        String subject = MessageFormat.format(messageConfig.getMailSubject(), config.getSiteName());
		send(email, subject, messageConfig.getMailContent(), params);
	}

	/**
	 * 发送会员注册邮件
	 * 
	 * @param member
	 *            会员
	 */
	public static void sendRegisterMemberMail(String code,String email) {
		MessageConfig messageConfig = new MessageConfig().dao().findByType(MessageConfig.Type.REGISTER.ordinal());
		if (messageConfig == null) {
			return;
		}
		Config config = SystemUtils.getConfig();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("code", code);
		String subject = MessageFormat.format(messageConfig.getMailSubject(), config.getSiteName());
		send(email, subject, messageConfig.getMailContent(), params);
	}
}
