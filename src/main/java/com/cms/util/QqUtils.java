package com.cms.util;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.HttpKit;

public class QqUtils {

    /** QQ互联https://connect.qq.com */
	public static final String appId = "";   
	public static final String appKey = "";  
    
	private static final String GET_AUTHORIZE_URL = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=%s&redirect_uri=%s&scope=get_user_info";
	
	public static String getAuthorizeUrl(String redirectUri){
	    String url = String.format(GET_AUTHORIZE_URL, appId,redirectUri);
	    System.out.println("url=="+url);
	    return url;
	}
	
	private static final String GET_ACCESS_TOKEN_URL = "https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id=%s&client_secret=%s&code=%s&redirect_uri=%s";
	
	//获取access_token
	public static Map<String,Object> getOauth2Token(String code,String redirectUri){
	    String content = HttpKit.get(String.format(GET_ACCESS_TOKEN_URL, appId,appKey,code,redirectUri), null);
	    System.out.println("GET_ACCESS_TOKEN_URL=="+content);
	    return JSONObject.parseObject(content, Map.class);
	}
	
	//获取openId
	private static final String GET_OPENID_URL="https://graph.qq.com/oauth2.0/me?access_token=%s";
	public static Map<String,Object> getOpenId(String accessToken){
		String content = HttpKit.get(String.format(GET_OPENID_URL, accessToken), null);
	    System.out.println("GET_OPENID_URL=="+content);
	    return JSONObject.parseObject(content, Map.class);
	}
	
	//获取用户信息
	private static final String GET_USER_INFO = "https://graph.qq.com/user/get_user_info?access_token=%s&oauth_consumer_key=%s&openid=%s";
	public static Map<String,Object> getUserInfo(String accessToken,String openId){
		String content = HttpKit.get(String.format(GET_USER_INFO, accessToken,appId,openId), null);
	    System.out.println("GET_USER_INFO=="+content);
	    return JSONObject.parseObject(content, Map.class);
	}
}
