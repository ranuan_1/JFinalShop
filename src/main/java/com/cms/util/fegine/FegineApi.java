package com.cms.util.fegine;
/**
全国快递物流查询
 商品购买地址： https://market.aliyun.com/products/56928004/cmapi021863.html
 String host = "http://wuliu.market.alicloudapi.com"; //服务器
 String path = "/kdi"; //接口地址
 */


import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import com.alibaba.fastjson.JSONObject;
import com.cms.Config;
import com.cms.util.SystemUtils;

public class FegineApi {
    public static FegineResponse getInfo(String no,String type){
    	Config config = SystemUtils.getConfig();
        String host = "http://wuliu.market.alicloudapi.com";
        String path = "/kdi";
        String method = "GET";
        String appcode = config.getWuliu_code();  // !!! 替换这里填写你自己的AppCode 请在买家中心查看
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "APPCODE " + appcode); //格式为:Authorization:APPCODE 83359fd73fe11248385f570e3c139xxx
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("no", no);// !!! 请求参数 462587770684
        querys.put("type", type);// !!! 请求参数  zto
        try {
            HttpResponse response = HttpUtils.doGet(host, path, method, headers, querys);
            //System.out.println(response.toString()); //输出头部
            String json = EntityUtils.toString(response.getEntity());
            System.out.println(json); //输出json
            return JSONObject.parseObject(json,FegineResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
