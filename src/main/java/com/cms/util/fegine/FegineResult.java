package com.cms.util.fegine;

import java.util.List;

public class FegineResult {

	private String number;
	private String type;
	private List<FegineInfo> list;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<FegineInfo> getList() {
		return list;
	}
	public void setList(List<FegineInfo> list) {
		this.list = list;
	}
}
