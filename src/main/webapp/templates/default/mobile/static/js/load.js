function loadCart(){
	$.ajax({
        url: "/cart/view",
        type: "post",
        data: {},
        dataType: "json",
        success: function(res){
        	if(res.type=="success"){
        		if(res.data.quantity>0){
        			$("#cartQuantity").html(res.data.quantity);
        		}
       		}
        },
        error: function(res){
        }
    });
}
loadCart();

// js如下：
$("#searchkeyword").keypress(function(e){
    var key = $.trim($(this).val());
    if(e.keyCode === 13) {
        //  搜索工作
        $("#searchkeywordForm").submit();
    }
})
